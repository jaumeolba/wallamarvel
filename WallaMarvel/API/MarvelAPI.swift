//
//  MarvelAPI.swift
//  WallaMarvel
//
//  Created by Jaume Ollés Barberán on 14/04/2018.
//  Copyright © 2018 jolles. All rights reserved.
//

import Foundation
import CryptoSwift

class MarvelAPI {
    public static let publicKey = "bcb4861b3a7f5298013c3c496de5c6f9"
    public static let privateKey = "0b96499b113e565ef289ece8c2a362b5a01216ca"
    public static let host = "http://gateway.marvel.com/v1/public"
    public static let baseURL = URL(string: MarvelAPI.host)!
    public static let charactersEndpoint = "/characters"
    
    public static func hash(for timestamp: String) -> String {
        let str = String(timestamp) + MarvelAPI.privateKey + MarvelAPI.publicKey
        return str.md5()
    }
}
