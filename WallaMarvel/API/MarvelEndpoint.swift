//
//  MarvelEndpoint.swift
//  WallaMarvel
//
//  Created by Jaume Ollés Barberán on 14/04/2018.
//  Copyright © 2018 jolles. All rights reserved.
//

import Foundation
import Moya

enum MarvelEndpoint {
    case characters(offset: Int)
}

extension MarvelEndpoint: TargetType {
    

    
    var baseURL: URL { return MarvelAPI.baseURL }
    
    var headers: [String : String]? {
        return [:]
    }
    
    var path: String {
        return MarvelAPI.charactersEndpoint
    }
    
    var method: Moya.Method {
        return .get
    }
    
    var parameters: [String: Any]? {
        switch self {
        case .characters(let offset):
            let timeStamp = String(Date().timeIntervalSince1970)
            var parameters = [String : Any]()
            parameters["ts"] = timeStamp
            parameters["apikey"] = MarvelAPI.publicKey
            parameters["hash"] = MarvelAPI.hash(for: timeStamp)
            parameters["offset"] = offset
            return parameters
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        return .requestParameters(parameters: parameters ?? [:], encoding: parameterEncoding)
    }
    
    var parameterEncoding: ParameterEncoding {
        return URLEncoding.default
    }
}
