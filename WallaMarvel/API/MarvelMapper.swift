//
//  MarvelMapper.swift
//  WallaMarvel
//
//  Created by Jaume Ollés Barberán on 14/04/2018.
//  Copyright © 2018 jolles. All rights reserved.
//

import Foundation
import Unbox

struct MarvelResponse {
    let code: Int
    let data: [MarvelCharacter]
}

struct MarvelCharacter: Equatable {
    let id: Int
    let name: String
    let description: String
    let modified: String
    let thumbnailObj: MarvelCharacterThumbnail?
    var thumbnail: URL? {
        get {
            guard let thumb = thumbnailObj else { return nil }
            return URL(string: thumb.path + "." + thumb.ext)
        }
    }
    
    static func == (lhs: MarvelCharacter, rhs: MarvelCharacter) -> Bool {
        guard lhs.id == rhs.id else { return false }
        guard lhs.name == rhs.name else { return false }
        guard lhs.description == rhs.description else { return false }
        guard lhs.modified == rhs.modified else { return false }
        guard lhs.thumbnail == rhs.thumbnail else { return false }
        return true
    }
}

struct MarvelCharacterThumbnail {
    let path: String
    let ext: String
}

extension MarvelResponse: Unboxable {
    public init(unboxer: Unboxer) throws  {
        code = try unboxer.unbox(key: "code")
        data = try unboxer.unbox(keyPath: "data.results")
    }
}

extension MarvelCharacter: Unboxable {
    public init(unboxer: Unboxer) throws  {
        id = try unboxer.unbox(key: "id")
        name = try unboxer.unbox(key: "name")
        description = try unboxer.unbox(key: "description")
        modified = try unboxer.unbox(key: "modified")
        thumbnailObj = unboxer.unbox(key: "thumbnail")
    }
}

extension MarvelCharacterThumbnail: Unboxable {
    public init(unboxer: Unboxer) throws  {
        path = try unboxer.unbox(key: "path")
        ext = try unboxer.unbox(key: "extension")
    }
}

class MarvelMapper {
    static func mapResponse(data: Data, onSuccess: (([MarvelCharacter]) -> Void), onError: ((Error) -> Void)) {
        do {
            let response : MarvelResponse = try unbox(data:data)
            onSuccess(response.data)
        } catch {
            onError(error)
        }
    }
}
