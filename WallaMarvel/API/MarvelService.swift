//
//  MarvelService.swift
//  WallaMarvel
//
//  Created by Jaume Ollés Barberán on 14/04/2018.
//  Copyright © 2018 jolles. All rights reserved.
//

import Foundation
import Moya
import RxSwift
import Unbox

protocol MarvelServiceProtocol {
    func retrieveCharacters(offset: Int, onSuccess: @escaping (([MarvelCharacter]) -> Void), onError: @escaping ((Error) -> Void))
}

class MarvelService: MarvelServiceProtocol {
    
    let disposeBag = DisposeBag()
    let provider: MoyaProvider<MarvelEndpoint>
    
    
    init() {
        #if DEBUG
        provider = MoyaProvider<MarvelEndpoint>(plugins: [NetworkLoggerPlugin.init(verbose: true, cURL: true)])
        #else
        provider = MoyaProvider<MarvelEndpoint>()
        #endif
    }
    
    func retrieveCharacters(offset: Int = 0, onSuccess: @escaping (([MarvelCharacter]) -> Void), onError: @escaping ((Error) -> Void)) {
        provider.rx.request(.characters(offset: offset)).filterSuccessfulStatusCodes().retry(3).subscribe(onSuccess: { (response) in
            MarvelMapper.mapResponse(data: response.data, onSuccess: onSuccess, onError: onError)
        }, onError: onError).disposed(by: disposeBag)
    }
}


