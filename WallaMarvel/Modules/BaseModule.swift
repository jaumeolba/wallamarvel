//
//  BaseModule.swift
//  WallaMarvel
//
//  Created by Jaume Ollés Barberán on 15/04/2018.
//  Copyright © 2018 jolles. All rights reserved.
//

import Foundation
import UIKit

enum PresentationStyle {
    case modal(type: UIModalPresentationStyle?, navigationController: Bool)
    case push(hidesBottomBar: Bool)
}

protocol BaseModule: class {
    weak var presentedViewController : UIViewController? { get set }
    func getView() -> UIViewController?
    
    func show(_ window: UIWindow)
    func push(from: UIViewController)
    func present(from: UIViewController)
}

extension BaseModule {
    func show(_ window: UIWindow) {
        guard let view = getView() else {
            return
        }
        let navigationController = UINavigationController.init(rootViewController: view)
        navigationController.navigationBar.isTranslucent = false
        navigationController.navigationBar.tintColor = UIColor.red
        navigationController.navigationBar.barTintColor = UIColor.white
        window.rootViewController = navigationController
    }
    
    func show(from: UIViewController, presentationType:PresentationStyle, transitionDelegate: UINavigationControllerDelegate? = nil) {
        guard let view = getView() else {
            return
        }
        presentModule(fromView: from, view: view, presentationType: presentationType, transitionDelegate: transitionDelegate)
    }
    
    func presentModule(fromView sourceView: UIViewController, view: UIViewController, presentationType:PresentationStyle, transitionDelegate: UINavigationControllerDelegate? = nil) {
        
        switch presentationType {
        case .modal(let type, let navigation):
            
            let controller = navigation ? UINavigationController.init(rootViewController: view) : view
            
            if let controller = controller as? UINavigationController {
                controller.delegate = transitionDelegate
            }
            
            if let presentedViewController = presentedViewController {
                presentedViewController.dismiss(animated: false, completion: nil)
            }
            
            if let type = type {
                controller.modalPresentationStyle = type
                if type == .overCurrentContext {
                    sourceView.definesPresentationContext = true
                    sourceView.providesPresentationContextTransitionStyle = true
                }
            } else {
                switch Device.type {
                case .tablet:
                    controller.modalPresentationStyle = .formSheet
                default:
                    controller.modalPresentationStyle = .fullScreen
                }
            }
            self.presentedViewController = controller
            sourceView.present(controller, animated: true, completion: nil)
        case .push(let hidesBottomBar):
            guard let navigationController = sourceView.navigationController else {
                return
            }
            self.presentedViewController = navigationController
            navigationController.delegate = transitionDelegate
            view.hidesBottomBarWhenPushed = hidesBottomBar
            navigationController.pushViewController(view, animated: true)
            
        }
    }
    
    func push(from: UIViewController) {
        show(from: from, presentationType: .push(hidesBottomBar: false), transitionDelegate: nil)
    }
    
    func present(from: UIViewController) {
        show(from: from, presentationType: .modal(type: nil, navigationController: false), transitionDelegate: nil)
    }
}
