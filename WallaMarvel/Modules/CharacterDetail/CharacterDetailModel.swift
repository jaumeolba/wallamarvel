//
//  CharacterDetailModel.swift
//  WallaMarvel
//
//  Created by Jaume Ollés Barberán on 15/04/2018.
//  Copyright © 2018 jolles. All rights reserved.
//

import Foundation

struct CharacterDetailModel {
    let id: Int
    let name: String
    let description: String
    var thumbnail: URL?
    
    init(_ marvelCharacter: MarvelCharacter) {
        self.id = marvelCharacter.id
        self.name = marvelCharacter.name
        self.description = marvelCharacter.description
        self.thumbnail = marvelCharacter.thumbnail
    }
}
