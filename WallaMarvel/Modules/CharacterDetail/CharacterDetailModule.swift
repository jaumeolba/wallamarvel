//
//  CharacterDetailModule.swift
//  WallaMarvel
//
//  Created by Jaume Ollés Barberán on 15/04/2018.
//  Copyright © 2018 jolles. All rights reserved.
//

import Foundation
import UIKit

class CharacterDetailModule: BaseModule {
    
    weak var presentedViewController : UIViewController?
    
    let character: MarvelCharacter
    
    init(character: MarvelCharacter) {
        self.character = character
    }
    
    func loadDependencies(view: Any) {
        guard let view = view as? CharacterDetailView else { return }
        view.viewModel = CharacterDetailViewModel(model: CharacterDetailModel.init(character))
    }
    
    func getView() -> UIViewController? {
        if let view = UIStoryboard.init(resourceKey: .CharacterDetail)?.view(ResourceKey.View.CharacterDetailView) as? CharacterDetailView {
            loadDependencies(view: view)
            return view
        }
        return nil
    }
    
    
}
