//
//  CharacterDetailView.swift
//  WallaMarvel
//
//  Created by Jaume Ollés Barberán on 15/04/2018.
//  Copyright © 2018 jolles. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import Alamofire
import SDWebImage

class CharacterDetailView: UIViewController {
    
    let disposeBag = DisposeBag()
    var viewModel: CharacterDetailViewModel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var decription: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bindViewModel()
        image.clipsToBounds = true
    }
    
    func bindViewModel() {
        viewModel.model.asObservable().bind { (characterDetail) in
            self.image.sd_setImage(with: characterDetail.thumbnail, placeholderImage: UIImage.init(named: ResourceKey.Asset.CharacterPlaceholder.rawValue))
            self.name.text = characterDetail.name
            self.decription.text = characterDetail.description
            }.disposed(by: disposeBag)
    }
}
