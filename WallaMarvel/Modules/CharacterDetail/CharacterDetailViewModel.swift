//
//  CharacterDetailViewModel.swift
//  WallaMarvel
//
//  Created by Jaume Ollés Barberán on 15/04/2018.
//  Copyright © 2018 jolles. All rights reserved.
//

import Foundation
import RxSwift

class CharacterDetailViewModel {
    
    let model: Variable<CharacterDetailModel>
    
    init(model: CharacterDetailModel) {
        self.model = Variable<CharacterDetailModel>(model)
    }
}
