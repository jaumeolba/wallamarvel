//
//  CharacterCell.swift
//  WallaMarvel
//
//  Created by Jaume Ollés Barberán on 14/04/2018.
//  Copyright © 2018 jolles. All rights reserved.
//

import Foundation
import UIKit
import Skeleton

class CharacterCell: UICollectionViewCell {
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var name: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.layer.cornerRadius = 10.0
        self.contentView.layer.masksToBounds = true
        self.contentView.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.3).cgColor
        self.contentView.layer.borderWidth = 1.0
        image.clipsToBounds = true
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        name.text = nil
        image.image = nil
        image.sd_setImage(with: nil)
    }
}

class CharacterPlaceholderCell: UICollectionViewCell {
    
    @IBOutlet weak var textPlaceholder: GradientContainerView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.layer.cornerRadius = 10.0
        self.contentView.layer.masksToBounds = true
        self.contentView.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.3).cgColor
        self.contentView.layer.borderWidth = 1.0
        
        let baseColor = textPlaceholder.backgroundColor!
        for gradientLayer in gradientLayers {
            let colors = [baseColor.cgColor,
                                baseColor.brightened(by: 0.93).cgColor,
                                baseColor.cgColor]
            gradientLayer.colors = colors
        }
        textPlaceholder.layer.cornerRadius = textPlaceholder.frame.height / 2
        textPlaceholder.layer.masksToBounds = true
        self.slide(to: .right)
    }
}

extension CharacterPlaceholderCell: GradientsOwner {
    var gradientLayers: [CAGradientLayer] {
        get {
            return [textPlaceholder.gradientLayer]
        }
    }
}

extension UIColor {
    func brightened(by factor: CGFloat) -> UIColor {
        var h: CGFloat = 0, s: CGFloat = 0, b: CGFloat = 0, a: CGFloat = 0
        getHue(&h, saturation: &s, brightness: &b, alpha: &a)
        return UIColor(hue: h, saturation: s, brightness: b * factor, alpha: a)
    }
}

