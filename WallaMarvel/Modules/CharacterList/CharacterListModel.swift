//
//  CharacterListModel.swift
//  WallaMarvel
//
//  Created by Jaume Ollés Barberán on 14/04/2018.
//  Copyright © 2018 jolles. All rights reserved.
//

import Foundation
import RxCocoa

enum CharacterListModel: Equatable {
    case loading
    case error(error: Error)
    case empty(message: String)
    case data(characters: [MarvelCharacter])
    
    static func == (lhs: CharacterListModel, rhs: CharacterListModel) -> Bool {
        switch (lhs, rhs) {
        case (.loading, .loading):
            return true
        case (.empty, .empty):
            return true
        case (.error, .error):
            return true
        case (.data(let leftData), .data(let rightData)):
            return leftData == rightData
        default:
            return false
        }
    }
}

protocol MarvelCharacterCellModel {
    var name: String { get }
    var thumbnail: URL? { get }
}

extension MarvelCharacter: MarvelCharacterCellModel {
    
}
