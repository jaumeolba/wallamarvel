//
//  CharactersListModule.swift
//  WallaMarvel
//
//  Created by Jaume Ollés Barberán on 15/04/2018.
//  Copyright © 2018 jolles. All rights reserved.
//

import Foundation
import UIKit

class CharacterListModule: BaseModule {
    
    weak var presentedViewController : UIViewController?
    
    func loadDependencies(view: Any) {
        guard let view = view as? CharacterListView else { return }
        view.viewModel = CharacterListViewModel()
    }
    
    func getView() -> UIViewController? {
        if let view = UIStoryboard.init(resourceKey: .CharactersList)?.view(ResourceKey.View.CharacterListView) as? CharacterListView {
            loadDependencies(view: view)
            return view
        }
        return nil
    }
    
    
}
