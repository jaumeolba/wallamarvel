//
//  CharacterListView.swift
//  WallaMarvel
//
//  Created by Jaume Ollés Barberán on 14/04/2018.
//  Copyright © 2018 jolles. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import CCBottomRefreshControl

//ViewController & Setup
class CharacterListView: UIViewController {
    
    var viewModel: CharacterListViewModelProtocol!
    var searchController: UISearchController!
    let disposeBag = DisposeBag()
    
    @IBOutlet weak var messageView: MessageView!
    @IBOutlet weak var collectionView: UICollectionView!
    var refreshControl = UIRefreshControl()
    var bottomRefreshControl = UIRefreshControl()
    var charactersDataSource = CharactersListDataSource()
    var placeholderDataSource = CharactersListPlaceholderDataSource()

    func setLogoAsTitle() {
        let imageView = UIImageView.init(image: UIImage.init(resourceKey: .NavBarLogo))
        navigationItem.titleView = imageView
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureSearchBar()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLogoAsTitle()
        prepareCollectionView()
        subscribeToSearchBarChanges()
        bindViewModel()
        inititalLoad()
    }
    
    func inititalLoad() {
        viewModel.reloadCharacters()
    }
    
    func prepareCollectionView() {
        collectionView.delegate = self
        refreshControl.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
        collectionView.refreshControl = refreshControl
        bottomRefreshControl.addTarget(self, action: #selector(pullForMore), for: .valueChanged)
        collectionView.bottomRefreshControl = bottomRefreshControl
    }

    func bindViewModel() {
        viewModel.model.asObservable().subscribe(onNext: { (model) in
            switch model {
            case .error(let error):
                self.showError(error)
            case .empty(let message):
                self.showEmptyView(message)
            case .data(let characters):
                self.showData(characters)
            case .loading:
                self.showLoading()
            }
        }).disposed(by: disposeBag)
    }
}

//Actions
extension CharacterListView {
    @objc func pullToRefresh() {
        viewModel.reloadCharacters()
    }
    
    @objc func pullForMore() {
        viewModel.requestMoreCharacters()
    }
}

//States
extension CharacterListView {
    func showError(_ error: Error) {
        refreshControl.endRefreshing()
        bottomRefreshControl.endRefreshing()
        showData([])
        messageView.image.image = UIImage(resourceKey: .Error)
        messageView.text.text = error.localizedDescription
        messageView.isHidden = false
    }
    
    func showEmptyView(_ message: String) {
        refreshControl.endRefreshing()
        bottomRefreshControl.endRefreshing()
        showData([])
        messageView.image.image = UIImage(resourceKey: .Empty)
        messageView.text.text = message
        messageView.isHidden = false
    }
    
    func showData(_ characters: [MarvelCharacter]) {
        refreshControl.endRefreshing()
        bottomRefreshControl.endRefreshing()
        self.charactersDataSource.data = characters
        self.collectionView.dataSource = self.charactersDataSource
        self.collectionView.reloadData()
        messageView.isHidden = true
        collectionView.isHidden = false
    }
    
    func showLoading() {
        self.collectionView.dataSource = self.placeholderDataSource
        self.collectionView.reloadData()
        messageView.isHidden = true
        collectionView.isHidden = false
    }
}

//Delegate
extension CharacterListView: UICollectionViewDelegateFlowLayout, Columnable {
    
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        self.collectionView.collectionViewLayout.invalidateLayout()
    }
    func numberOfItemsPerRow(indexPath: IndexPath?) -> Int {
        switch (Device.type, Device.orientation) {
        case (.phone, .portrait):
            return 2
        case (.phone, .landscape):
            return 4
        case (.tablet, .portrait):
            return 4
        case (.tablet, .landscape):
            return 6
        }
    }
    
    func cellAspectRatio() -> Float {
        return 0.7
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let character = viewModel.characterAt(indexPath.row) else { return }
        Navigator().showCharacterDetailView(from: self, character: character)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return self.cellSize(collectionView, layout:collectionViewLayout, sizeForItemAt: indexPath)
    }
    
}

//Search
extension CharacterListView: UISearchControllerDelegate, UISearchBarDelegate {
    
    internal func configureSearchBar() {
        self.searchController = UISearchController(searchResultsController:  nil)
        self.searchController.delegate = self
        self.searchController.searchBar.delegate = self
        self.searchController.searchBar.tintColor = UIColor.red
        self.searchController.searchBar.barTintColor = UIColor.red
        self.searchController.searchBar.backgroundColor = UIColor.white
        self.searchController.hidesNavigationBarDuringPresentation = false
        self.searchController.searchBar.placeholder = String(resourceKey: .SearchHint)
        if #available(iOS 9.1, *) {
            self.searchController.obscuresBackgroundDuringPresentation = false
        }
        
        if #available(iOS 11.0, *) {
            self.navigationItem.searchController = self.searchController
            self.navigationItem.hidesSearchBarWhenScrolling = false
        }
    }
    
    func subscribeToSearchBarChanges() {
        searchController.searchBar
            .rx.text
            .orEmpty
            .skip(1)
            .debounce(0.3, scheduler: MainScheduler.instance)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] query in
                self.viewModel.filter(by: query)
            })
            .disposed(by: disposeBag)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        viewModel.filter(by: "")
        self.searchController.searchBar.showsCancelButton = false
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchController.searchBar.showsCancelButton = true
    }
}
