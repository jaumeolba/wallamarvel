//
//  CharacterListViewModel.swift
//  WallaMarvel
//
//  Created by Jaume Ollés Barberán on 14/04/2018.
//  Copyright © 2018 jolles. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol CharacterListViewModelProtocol {
    var model: Variable<CharacterListModel> { get set }
    func reloadCharacters()
    func requestMoreCharacters()
    func characterAt(_ index: Int) -> MarvelCharacter?
    func filter(by query: String)
}

class CharacterListViewModel {

    var model = Variable<CharacterListModel>(.loading)
    var characters = [MarvelCharacter]()
    var query = ""

    let marvelService: MarvelServiceProtocol
    
    init(marvelService: MarvelServiceProtocol = MarvelService()) {
        self.marvelService = marvelService
    }
}

extension CharacterListViewModel: CharacterListViewModelProtocol {
    
    func reloadCharacters() {
        characters.removeAll()
        requestMoreCharacters()
    }
    
    func requestMoreCharacters() {
        if characters.isEmpty {
            model.value = .loading
        }
        marvelService.retrieveCharacters(offset: self.characters.count, onSuccess: { (characters) in
            self.characters.append(contentsOf: characters)
            self.processCharacters()
        }) { (error) in
            self.model.value = .error(error: error)
        }
    }
    
    func characterAt(_ index: Int) -> MarvelCharacter? {
        let filteredCharacters = filterCharacters()
        guard filteredCharacters.count > index else { return nil }
        return filteredCharacters[index]
    }
    
    func filter(by query: String) {
        self.query = query
        processCharacters()
    }
}

extension CharacterListViewModel {
    
    private func filterCharacters() -> [MarvelCharacter] {
        return self.characters.filter { (character) -> Bool in
            return character.name.hasPrefix(self.query) || character.name.contains(self.query)
        }
    }
    
    private func processCharacters() {
        let filteredCharacters = filterCharacters()
        if filteredCharacters.isEmpty {
            self.model.value = .empty(message: String(resourceKey: .NoCharactersAvailable))
        } else {
            self.model.value = .data(characters: filteredCharacters)
        }
    }
}
