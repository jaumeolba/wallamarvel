//
//  CharactersListDataSource.swift
//  WallaMarvel
//
//  Created by Jaume Ollés Barberán on 14/04/2018.
//  Copyright © 2018 jolles. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SDWebImage

class CharactersListDataSource: NSObject{
    
    var data: [MarvelCharacterCellModel] = []
}

extension CharactersListDataSource: UICollectionViewDataSource {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CharacterCell", for: indexPath) as? CharacterCell, data.count > indexPath.row else { return UICollectionViewCell()}
        let element = data[indexPath.row]
        cell.name.text = element.name
        cell.image.sd_setImage(with: element.thumbnail, placeholderImage: UIImage.init(named: ResourceKey.Asset.CharacterPlaceholder.rawValue))
        return cell
    }
}

class CharactersListPlaceholderDataSource: NSObject, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CharacterPlaceholderCell", for: indexPath) as? CharacterPlaceholderCell else { return UICollectionViewCell()}
        return cell
    }
}
