//
//  Navigator.swift
//  WallaMarvel
//
//  Created by Jaume Ollés Barberán on 15/04/2018.
//  Copyright © 2018 jolles. All rights reserved.
//

import Foundation
import UIKit

class Navigator {
    internal var window: UIWindow? {
        get {
            return (UIApplication.shared.delegate as! AppDelegate).window
        }
    }
    
    func showCharactersListView() {
        guard let window = window else {
            return
        }
        window.backgroundColor = UIColor.white
        CharacterListModule().show(window)
    }
    
    func showCharacterDetailView(from viewController: UIViewController, character: MarvelCharacter) {
        guard let window = window else {
            return
        }
        CharacterDetailModule(character: character).push(from: viewController)
    }
}
