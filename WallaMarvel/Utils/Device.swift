//
//  Device.swift
//  WallaMarvel
//
//  Created by Jaume Ollés Barberán on 14/04/2018.
//  Copyright © 2018 jolles. All rights reserved.
//

import Foundation
import UIKit

enum DeviceType {
    case phone, tablet
}

enum DeviceOrientation : String {
    case portrait, landscape
}

class Device {
    public static var orientation : DeviceOrientation {
        switch UIDevice.current.orientation  {
        case .landscapeLeft, .landscapeRight:
            return .landscape
        case .portrait, .portraitUpsideDown:
            return .portrait
        default:
            if UIApplication.shared.statusBarOrientation.isLandscape {
                return .landscape
            } else {
                return .portrait
            }
        }
    }
    
    public static var type : DeviceType {
        switch UIDevice.current.userInterfaceIdiom  {
        case .pad:
            return .tablet
        case .phone:
            return .phone
        default:
            return .phone
        }
    }
}
