//
//  MessageView.swift
//  WallaMarvel
//
//  Created by Jaume Ollés Barberán on 14/04/2018.
//  Copyright © 2018 jolles. All rights reserved.
//

import Foundation
import UIKit

class MessageView: UIView {
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var text: UILabel!
}
