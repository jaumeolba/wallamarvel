//
//  Resource.swift
//  WallaMarvel
//
//  Created by Jaume Ollés Barberán on 15/04/2018.
//  Copyright © 2018 jolles. All rights reserved.
//

import UIKit

extension String {
    
    /**
     Inits localized String with resourceKey param
     - parameter resourceKey: ResourceKey.Translation value
     */
    init(resourceKey key: ResourceKey.Translation, defaultValue: String? = nil) {
        self = NSLocalizedString(key.rawValue, comment: key.rawValue)
        let customString = NSLocalizedString(key.rawValue, tableName: "InfoPlist", bundle: Bundle.main, value: key.rawValue, comment: key.rawValue)
        if customString != key.rawValue {
            self = customString
        }
        
        if self == key.rawValue, let defaultString = defaultValue {
            self = defaultString
        }
    }
    
    /**
     Inits UIImage with resourceKey param and args
     - parameter resourceKey: ResourceKey.Asset value
     - parameter index: Index arguments to format ResourceKey
     */
    init(resourceKey key: ResourceKey.Translation, index: Int, defaultValue: String? = nil) {
        let keyValue = String.init(format: key.rawValue, index)
        self = NSLocalizedString(keyValue, comment: keyValue)
        let customString = NSLocalizedString(keyValue, tableName: "InfoPlist", bundle: Bundle.main, value: keyValue, comment: keyValue)
        if customString != keyValue {
            self = customString
        }
        
        if self == keyValue, let defaultString = defaultValue {
            self = defaultString
        }
    }
}


extension UIImage {
    
    /**
     Inits UIImage with resourceKey param
     - parameter resourceKey: ResourceKey.Asset value
     */
    convenience init?(resourceKey: ResourceKey.Asset) {
        self.init(named: resourceKey.rawValue)
    }
    
    /**
     Inits UIImage with resourceKey param and args
     - parameter resourceKey: ResourceKey.Asset value
     - parameter index: Index arguments to format UIImage
     */
    convenience init?(resourceKey: ResourceKey.Asset, index: Int) {
        self.init(named: String.init(format: resourceKey.rawValue, index))
    }
    
    /**
     Inits UIImage with resourceKey param and args
     - parameter resourceKey: ResourceKey.Asset value
     - parameter code: Code arguments to format UIImage
     */
    convenience init?(resourceKey: ResourceKey.Asset, code: String) {
        self.init(named: String.init(format: resourceKey.rawValue, code))
    }
}

extension UINib {
    
    /**
     Inits UINib with resourceKey param
     - parameter resourceKey: ResourceKey.Xib value
     */
    convenience init?(resourceKey: ResourceKey.Xib) {
        self.init(nibName: resourceKey.rawValue, bundle: nil)
    }
    
}

extension UIStoryboard {
    
    /**
     Inits UIStoryboard with resourceKey param
     - parameter resourceKey: ResourceKey.Storyboard value
     */
    convenience init?(resourceKey: ResourceKey.Storyboard) {
        self.init(name: resourceKey.rawValue,bundle: Bundle.main)
    }
    
    /**
     Returns corresponding view of resourceKey in the storyboard (if exists)
     - parameter resourceKey: ResourceKey.View value
     - returns: View from storyboard
     */
    func view(_ resourceKey: ResourceKey.View) -> UIViewController? {
        return self.instantiateViewController(withIdentifier: resourceKey.rawValue)
    }
}

extension UIViewController {
    /**
     Get UIViewController with storyboardKey and viewKey params
     - parameter storyboardKey: ResourceKey.Storyboard value
     - parameter viewKey: ResourceKey.View value
     */
    static func fromResourceKey(_ storyboardKey: ResourceKey.Storyboard, viewKey: ResourceKey.View) -> UIViewController? {
        return UIStoryboard(resourceKey: storyboardKey)?.view(viewKey)
    }
}
