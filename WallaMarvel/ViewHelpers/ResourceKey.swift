//
//  ResourceKey.swift
//  WallaMarvel
//
//  Created by Jaume Ollés Barberán on 15/04/2018.
//  Copyright © 2018 jolles. All rights reserved.
//

import Foundation
/**
 Resource keys avaiable for the app. Use this keys to access:
 - Images (Asset)
 - Storyboards (Storyboard)
 - Views in  corresponding Storyboards (View)
 */
enum ResourceKey {
    enum Asset : String {
        case
        NavBarLogo = "NavBarLogo",
        MarvelLogo = "MarvelLogo",
        CharacterPlaceholder = "CharacterPlaceholder",
        Error = "Error",
        Empty = "Empty"
    }
    
    enum Storyboard : String {
        case
        CharactersList = "CharactersList",
        CharacterDetail = "CharacterDetail"
    }
    
    enum Xib : String {
        case
        CategoryListCell = "CharacterCell"
    }
    
    enum View : String {
        case
        CharacterListView = "CharacterListView",
        CharacterDetailView = "CharacterDetailView"
    }
    
    enum Translation : String {
        case
        NoCharactersAvailable = "NoCharactersAvailable",
        SearchHint = "SearchHint"
    }
}
