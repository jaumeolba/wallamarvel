//
//  UICollectionViewColumnable.swift
//  WallaMarvel
//
//  Created by Jaume Ollés Barberán on 14/04/2018.
//  Copyright © 2018 jolles. All rights reserved.
//

import Foundation
import UIKit

protocol Columnable : UICollectionViewDelegateFlowLayout {
    func numberOfItemsPerRow(indexPath: IndexPath?) -> Int
    func cellAspectRatio() -> Float
}

extension UICollectionViewDelegateFlowLayout where Self: Columnable {
    
    func cellSize(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let numberOfItems = numberOfItemsPerRow(indexPath: indexPath)
        var totalSpace = collectionView.bounds.width
        if let flowLayout = collectionViewLayout as? UICollectionViewFlowLayout {
            totalSpace = flowLayout.sectionInset.left
                + flowLayout.sectionInset.right
                + (flowLayout.minimumInteritemSpacing * CGFloat(numberOfItems - 1))
        }
        let width = (collectionView.bounds.width - totalSpace) / CGFloat(numberOfItems)
        let height = CGFloat(width)/CGFloat(cellAspectRatio())
        
        return CGSize(width: width, height: height)
    }
}
