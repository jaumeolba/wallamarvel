//
//  CharacterDetailViewTests.swift
//  WallaMarvelTests
//
//  Created by Jolles on 22/04/2018.
//  Copyright © 2018 jolles. All rights reserved.
//

import XCTest
@testable import WallaMarvel
import Quick
import Nimble
import RxSwift

class CharacterDetailViewTests: QuickSpec {
    
    override func spec() {
        var subject: CharacterDetailView!
        var viewModel: CharacterDetailViewModel!
        var model: CharacterDetailModel!
        describe("CharacterDetailViewSpec") {
            beforeEach {
                subject = UIStoryboard.init(resourceKey: .CharacterDetail)?.view(ResourceKey.View.CharacterDetailView) as! CharacterDetailView
                model = CharacterDetailModel(CharacterMock.character(for: 1))
                viewModel = CharacterDetailViewModel(model: model)
                subject.viewModel = viewModel
                subject.loadView()
            }
            
            context("when the view bonds to viewModel", {
                it("should match charcater fields", closure: {
                    subject.bindViewModel()
                    XCTAssertEqual(subject.name.text, model.name)
                    XCTAssertEqual(subject.decription.text, model.description)
                })
            })
        }
    }
}
