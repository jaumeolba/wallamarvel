//
//  CharacterListMock.swift
//  WallaMarvelTests
//
//  Created by Jolles on 22/04/2018.
//  Copyright © 2018 jolles. All rights reserved.
//

import Foundation
@testable import WallaMarvel
import RxSwift

class CharacterListViewModelMock: CharacterListViewModelProtocol {
    
    var function: String!
    
    var model: Variable<CharacterListModel> = Variable<CharacterListModel>(.loading)
    
    func reloadCharacters() {
        function = "reloadCharacters"
    }
    
    func requestMoreCharacters() {
        function = "requestMoreCharacters"
    }
    
    func characterAt(_ index: Int) -> MarvelCharacter? {
        return nil
    }
    
    func filter(by query: String) {
        function = "filter(by: \(query)"
    }
}
