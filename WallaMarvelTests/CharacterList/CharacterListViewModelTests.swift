//
//  CharacterListViewModelTests.swift
//  WallaMarvelTests
//
//  Created by Jolles on 22/04/2018.
//  Copyright © 2018 jolles. All rights reserved.
//

import XCTest
@testable import WallaMarvel
import Quick
import Nimble
import RxSwift

class CharacterListViewModelTests: QuickSpec {
    
    override func spec() {
        var subject: CharacterListViewModel!
        var model: Variable<CharacterListModel>
        var service: MarvelServiceProtocol!
        var characters: [MarvelCharacter]!
        describe("CharacterListViewModelSpec") {
            beforeEach {
                service = MarvelServiceMock()
                subject = CharacterListViewModel.init(marvelService: service)
                characters = []
            }
            
            context("when requests for characters and result is empty", {
                beforeEach {
                    service = MarvelServiceMock(characters: [])
                    subject = CharacterListViewModel.init(marvelService: service)
                    subject.reloadCharacters()
                }
                it("should set model to .empty", closure: {
                    XCTAssertEqual(subject.model.value, CharacterListModel.empty(message: ""))
                })
            })
            
            context("when requests for characters and gets error", {
                beforeEach {
                    service = MarvelServiceMock(error: MockError())
                    subject = CharacterListViewModel.init(marvelService: service)
                    subject.reloadCharacters()
                }
                it("should set model to .error", closure: {
                    XCTAssertEqual(subject.model.value, CharacterListModel.error(error: MockError()))
                })
            })
            
            context("when requests for characters and gets an array of Characters", {
                beforeEach {
                    characters = CharacterMock.getRandomCharacters(50)
                    service = MarvelServiceMock(characters: characters)
                    subject = CharacterListViewModel.init(marvelService: service)
                    subject.reloadCharacters()
                }
                it("should set model to .data(characters)", closure: {
                    XCTAssertEqual(subject.model.value, CharacterListModel.data(characters: characters))
                    XCTAssertEqual(subject.characters, characters)
                })
            })
            
            context("when ask for element", {
                beforeEach {
                    characters = CharacterMock.getRandomCharacters(50)
                    service = MarvelServiceMock(characters: characters)
                    subject = CharacterListViewModel.init(marvelService: service)
                    subject.characters = characters
                }
                it("should return the element at index", closure: {
                    for index in 0...49 {
                        XCTAssertEqual(subject.characterAt(index), CharacterMock.character(for: index + 1))
                    }
                    XCTAssertEqual(subject.characterAt(50), nil)
                })
            })
            
            context("when filter characters", {
                beforeEach {
                    characters = CharacterMock.getRandomCharacters(50)
                    service = MarvelServiceMock(characters: characters)
                    subject = CharacterListViewModel.init(marvelService: service)
                    subject.characters = characters
                    subject.filter(by: "1")
                }
                it("should return the characters that has query on its name", closure: {
                    subject.filter(by: "1")
                    switch subject.model.value {
                    case .data(let characters):
                        guard characters.count > 0 else {
                            XCTFail()
                            return
                        }
                        for character in characters {
                            guard character.name.contains("1") else {
                                XCTFail()
                                return
                            }
                        }
                    default:
                        XCTFail()
                    }
                })
            })
        }
    }
}
