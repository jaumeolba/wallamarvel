//
//  WallaMarvelTests.swift
//  WallaMarvelTests
//
//  Created by Jaume Ollés Barberán on 14/04/2018.
//  Copyright © 2018 jolles. All rights reserved.
//

import XCTest
@testable import WallaMarvel
import Quick
import Nimble
import RxSwift

class CharacterListViewTests: QuickSpec {
    
    override func spec() {
        var subject: CharacterListView!
        var viewModel: CharacterListViewModelMock!
        describe("CharacterListViewSpec") {
            beforeEach {
                subject = UIStoryboard.init(resourceKey: .CharactersList)?.view(ResourceKey.View.CharacterListView) as! CharacterListView
                subject.loadView()
                viewModel = CharacterListViewModelMock()
                subject.viewModel = viewModel
            }
            
            context("when is loaded", {
                beforeEach {
                    subject.viewDidLoad()
                }
                it("should call reloadCharacters", closure: {
                    expect(viewModel.function).to(equal("reloadCharacters"))
                    XCTAssertEqual(viewModel.model.value, CharacterListModel.loading)
                })
            })

            context("when pulls to refresh", {
                beforeEach {
                    subject.pullToRefresh()
                }
                it("should call reloadCharacters", closure: {
                    expect(viewModel.function).to(equal("reloadCharacters"))
                    XCTAssertEqual(viewModel.model.value, CharacterListModel.loading)
                })
            })
            
            context("when scrolls down", {
                beforeEach {
                    subject.pullForMore()
                }
                it("should call requestMoreCharacters", closure: {
                    expect(viewModel.function).to(equal("requestMoreCharacters"))
                })
            })
            
        }
    }    
}
