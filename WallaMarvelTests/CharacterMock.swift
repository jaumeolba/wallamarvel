//
//  CharacterMock.swift
//  WallaMarvelTests
//
//  Created by Jolles on 22/04/2018.
//  Copyright © 2018 jolles. All rights reserved.
//

import Foundation
@testable import WallaMarvel

class CharacterMock {
    
    static func getRandomCharacters(_ numOfCharacters: Int = 100) -> [MarvelCharacter] {
        var characters = [MarvelCharacter]()
        for index in 1...numOfCharacters {
            characters.append(MarvelCharacter(id: index, name: "Name \(index)", description: "Description \(index)", modified: "", thumbnailObj: nil))
        }
        return characters
    }
    
    static func character(for value: Int) -> MarvelCharacter {
        return MarvelCharacter(id: value, name: "Name \(value)", description: "Description \(value)", modified: "", thumbnailObj: nil)
    }
}
