//
//  ServiceMock.swift
//  WallaMarvelTests
//
//  Created by Jolles on 22/04/2018.
//  Copyright © 2018 jolles. All rights reserved.
//

import Foundation
@testable import WallaMarvel

class MockError: Error {
    
}

class MarvelServiceMock: MarvelServiceProtocol {
    
    let characters: [MarvelCharacter]?
    let error: Error?
    
    init(characters: [MarvelCharacter]? = nil, error: Error? = nil) {
        self.characters = characters
        self.error = error
    }
    
    func retrieveCharacters(offset: Int, onSuccess: @escaping (([MarvelCharacter]) -> Void), onError: @escaping ((Error) -> Void)) {
        if let error = self.error {
            onError(error)
        } else if let characters = self.characters {
            onSuccess(characters)
        } else {
            fatalError("No initialization value found for MarvelServiceMock")
        }
    }
}
